class Artisan < ApplicationRecord
    has_many :comments
    validates :name, presence: true,
                    length: { minimum: 4 }
    validates :location, presence: true, 
                    length: { minimum: 5 }
end
