class CommentsController < ApplicationController
    def create
        @artisan = Artisan.find(params[:artisan_id])
        @comment = @artisan.comments.create(comment_params)
        redirect_to artisan_path(@artisan)
      end
     
      private
        def comment_params
          params.require(:comment).permit(:commenter, :body)
        end
end
