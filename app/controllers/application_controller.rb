class ApplicationController < ActionController::Base
    before_action :set_unleash_context
    private
    def set_posthog_context
      @posthog = PostHog::Client.new({
        api_key: "hrcY0WIICFwr5nXGlDTSndJwnZ1W1exrSb19h_wvVqQ",
        on_error: Proc.new { |status, msg| print msg }
      })
    end

    protect_from_forgery with: :exception
    before_action :set_raven_context

    private
    def set_raven_context
      Raven.extra_context(params: params.to_unsafe_h, url: request.url)
    end

    before_action :set_unleash_context

    private
    def set_unleash_context
      @unleash_context = Unleash::Context.new(
        session_id: session.id,
        remote_address: request.remote_ip,
        app_name: Rails.env,
        user_id: session[:user_id]
      )
    end

    before_action :set_tracing_context
    def set_tracing_context
      OpenTracing.global_tracer = Jaeger::Client.build(service_name: 'artisans', host: 'jaeger.kencjohnston.com')
    end
end
