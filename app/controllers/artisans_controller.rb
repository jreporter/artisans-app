class ArtisansController < ApplicationController
    def index
        @artisans = Artisan.all
    end
    
    def show
        @artisan = Artisan.find(params[:id])
        Honeycomb.add_field("Artisan", @artisan.name)
        POSTHOG.capture({
            distinct_id: 'distinct id',
            event: 'Artisan Displayed',
            properties: {
                artisan_name: @artisan.name,
                artisan_id: @artisan.id
            }
        })
    end
    
    def random
        OpenTracing.start_span('random_artisan') do |scope|
            if UNLEASH.is_enabled? "random-artisan", @unleash_context
                @artisan = Artisan.all.sample  
            else
                @artisan = Artisan.first
            end
        end
    end

    def new
        @artisan = Artisan.new
    end
    
    def create
        @artisan = Artisan.new(artisan_params)

        if @artisan.save
            redirect_to @artisan
          else
            render 'new'
        end
    end

    def update
        @artisan = Artisan.find(params[:id])
       
        if @artisan.update(artisan_params)
          redirect_to @artisan
        else
          render 'edit'
        end
    end
    
    def edit
        @artisan = Artisan.find(params[:id])
    end

    def destroy
        @artisan = Artisan.find(params[:id])
        @artisan.destroy

        redirect_to artisans_path
    end

    private
        def artisan_params
            params.require(:artisan).permit(:name, :description, :location)
        end 
end
