# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Artisan.create(name: 'Kenny', location: 'Leawood', description: 'Kenny is alright')
Artisan.create(name: 'Cara', location: 'Leawood', description: 'Cara is wonderful')
Artisan.create(name: 'Rowan', location: 'Leawood', description: 'Ro is awesome')
Artisan.create(name: 'Everett', location: 'Leawood', description: 'Ev is funny')
Artisan.create(name: 'Petri', location: 'Leawood', description: 'Petri is asleep')