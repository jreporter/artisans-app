class AddLocationToArtisans < ActiveRecord::Migration[5.2]
  def change
    add_column :artisans, :location, :string
  end
end
