Rails.application.routes.draw do
  get 'welcome/index'

  resources :artisans do
    resources :comments
    collection do
      get 'random'
    end
  end
  
  root 'welcome#index'
end
