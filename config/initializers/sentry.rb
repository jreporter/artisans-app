Raven.configure do |config|
    config.dsn = 'https://749430edaf504764a3fe43a8ad7b50ca@sentry.io/1777336'
    config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
    config.excluded_exceptions = []
end