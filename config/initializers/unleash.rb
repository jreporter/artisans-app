Unleash.configure do |config|
    config.url      = 'https://gitlab.com/api/v4/feature_flags/unleash/12450055'
    config.app_name = Rails.env
    config.instance_id = 'JHa2aHTHix9xszhC1Nee'
    config.logger   = Rails.logger
    config.environment = Rails.env
  end
  
  UNLEASH = Unleash::Client.new