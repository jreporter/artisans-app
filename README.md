# Artisans App

Add your favorite artisan types with title, description and picture. Retrieve a random artisan. 

## Running Locally

### Requirements

Start a PG server:
```
pg_ctl -D /usr/local/var/postgres start
```

Run the application:
```
rails server
open http://localhost:3000/
```
## Demos
Install Vegata on Mac:
```
brew update && brew install vegeta
```
Send Traffic:
```
echo "GET $URL$" | vegeta attack -duration=4000s -connections=10 -rate=15 | tee results.bin | vegeta report
```

Edit the readme 2 
